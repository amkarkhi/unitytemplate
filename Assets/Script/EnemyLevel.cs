﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLevel : MonoBehaviour
{
    // Start is called before the first frame update
    public static EnemyLevel instance { get; private set; }

    public int score = 0;
    public int hp = 100;
    public bool GameRestart;
    public GameObject enemyPrefab;
    public float distance;
    public float maxY = 1;
    public float MinX = -1;
    public int enemyCount = 0;
    public AudioSource bkgmusic;
    public AudioClip explode;
    public AudioClip healthdamage;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            // SpriteRenderer spbkgrenderer =  new SpriteRenderer();
            // spbkgrenderer.sprite=bkgtile;            
        }
        else
        {
            Destroy(gameObject);
        }

    }
    private void Start()
    {
        Screen.SetResolution(800, 800, true, 60);
        Startthis();
        bkgmusic.Play();
        bkgmusic.loop = true;
    }
    void Startthis()
    {
        //should write level or LvlUP orr something
        float numberY = Random.Range(2, 7);
        float distanceX = Mathf.Abs(MinX * 2) / Random.Range(2, 11);
        for (float j = 0; j < numberY; j++)
        {
            for (float i = 0; i < Mathf.Abs(MinX * 2f); i += distanceX)
            {
                if (Random.Range(0, 15) > 2)
                {
                    Vector3 v = new Vector3(MinX + i, .1f + maxY - j * distance, 0) + transform.position;
                    Instantiate(enemyPrefab, v, Quaternion.identity);
                    enemyCount++;
                }
            }
        }
    }
    public void expldclip()
    {

        enemyCount--;
        score += 100;
        bkgmusic.PlayOneShot(explode, 1f);
    }
    public void heaalthhlostclip()
    {
        bkgmusic.PlayOneShot(healthdamage, .5f);
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(enemyCount);
        if (enemyCount <= 0)
            Startthis();
        bkgmusic.loop = true;
    }
}
