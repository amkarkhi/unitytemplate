﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class poop : MonoBehaviour
{
    // Start is called before the first frame update
    public float Speed;
    public Rigidbody2D Rb;
    void Start()
    {        
        Rb.velocity = -transform.up * Speed;
        Destroy(gameObject, 3);
    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D other)
    {
        playermovement player = other.GetComponent<playermovement>();
        if (player != null){
            player.Damage();            
            Destroy(gameObject);
        }
        bullet bulle  = other.GetComponent<bullet>();
        if(bulle!=null)
        {
            Destroy(gameObject);
        }
    }
}
