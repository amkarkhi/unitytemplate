﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getscore : MonoBehaviour
{
    // Start is called before the first frame update
    public Text score;
    void Start()
    {
        score =gameObject.GetComponent<Text>();
        
    }
    // Update is called once per frame
    void Update()
    {
        int scorenum=EnemyLevel.instance.score;
        score.text=scorenum.ToString();
    }
}
