﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    // Start is called before the first frame update
    public float Speed;
    public Rigidbody2D Rb;
    void Start()
    {
        Rb.velocity = transform.up * Speed;
        Destroy(gameObject, 3);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
        {
            
            enemy.Die();
            Destroy(gameObject);
        }
        poop poo = other.GetComponent<poop>();
        if (poo != null)
        {            
            Destroy(gameObject);            
        }
    }

}
