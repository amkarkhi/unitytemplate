﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovement : MonoBehaviour
{

    // Start is called before the first frame update
    private Animator animator;
    public float speed;
    public GameObject playerdmg;
    public Sprite dmgsprite;
    public SpriteRenderer rend;
    public GameObject damagehp;
    public Sprite origsprite;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.SetActive(true);
        float xm = Input.GetAxis("Horizontal");
        xm=transform.position.x+xm*speed*Time.deltaTime;
        float ym = Input.GetAxis("Vertical");
        ym =transform.position.y+ym*speed*Time.deltaTime;
        if ((xm < -1 || xm > 1))
            xm=transform.position.x;
        if ((ym < -1 || ym > 1))
            ym=transform.position.y;
        Vector3 position = new Vector3(xm, ym, 0.0f);        
        transform.position=position;
    }
    public void Damage()
    {
        // Instantiate(playerdmg, transform.position, Quaternion.identity);

        StartCoroutine(damageEffect());

    }
    IEnumerator damageEffect()
    {
        EnemyLevel.instance.hp-=10;
        Vector3 v =new Vector3 (transform.position.x+.1f,transform.position.y+ .2f,0f);
        Instantiate(damagehp,v,Quaternion.identity);
        EnemyLevel.instance.heaalthhlostclip();
        // origsprite = rend.sprite;
            rend.sprite = dmgsprite;
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(.1f);
            rend.enabled = false;
            yield return new WaitForSeconds(.1f);
            rend.enabled = true;
        }
            rend.sprite=origsprite;
            yield return new WaitForSeconds(.1f);
    }
}
