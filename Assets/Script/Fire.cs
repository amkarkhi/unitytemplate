﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform firepoint;
    public GameObject bulletprefab;
    public GameObject impactEffect;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            shoot();
        }
    }
    void shoot()
    {
        Instantiate(bulletprefab,firepoint.position,firepoint.rotation);
        Debug.Log("fire!");
        // if (hitinfo){
        //     Debug.Log(hitinfo.transform.name);
        //     Enemy enemy= hitinfo.transform.GetComponent<Enemy>();
        // RaycastHit2D hitinfo = Physics2D.Raycast(firepoint.position, firepoint.up);
        // Instantiate(impactEffect,hitinfo.point,Quaternion.identity);
        //     if(enemy!=null){
        //         enemy.Die();
        //     }
        // }


    }
}
