﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float limitMovement = 1;
    private int count = 0;
    public float movementSpeed = .5f;
    private float time;
    public GameObject explode;
    public GameObject poop;
    public AudioSource exploadeaaaudio;
    public GameObject score;
    private void Start()
    {
        exploadeaaaudio = GetComponent<AudioSource>();
        time = movementSpeed;
    }
    public void Die()
    {
        Instantiate(score,transform.position,Quaternion.identity);
        Instantiate(explode, transform.position, Quaternion.identity);        
        EnemyLevel.instance.expldclip();             
        Destroy(gameObject);
    }    
    private void Update()
    {
        if (time > 0)
        {
            time -= Time.deltaTime;
        }
        else
        {
            if (Random.Range(0, 12) < 1)
            {
                Instantiate(poop, transform.position, Quaternion.identity);
            }
            switch (count++)
            {
                case 0:
                    transform.position = transform.position + new Vector3(limitMovement, 0f, 0f);
                    break;
                case 1:
                    transform.position = transform.position + new Vector3(0f, -limitMovement, 0f);
                    break;
                case 2:
                    transform.position = transform.position + new Vector3(-limitMovement, 0f, 0f);
                    break;
                case 3:
                    transform.position = transform.position + new Vector3(0f, limitMovement, 0f);
                    count = 0;
                    break;
            }
            time = movementSpeed;
        }
    }
}
